#!/bin/bash

BASE_PATH=`dirname $0`
TRY_LOOP="20"

# Check if .env file exists.
if [ ! -e "$BASE_PATH/.env" ]; then
    if [ -e "$BASE_PATH/.env.example" ]; then
        cp $BASE_PATH/.env.example $BASE_PATH/.env
    else
        echo "ERROR: The .env file was not found or does not exist."
        exit 1
    fi
fi

# Чтение переменных из файла.
read_var() {
    if [ -z "$1" ]; then
        echo "environment variable name is required"
        return
    fi

    local ENV_FILE='.env'
    if [ ! -z "$2" ]; then
        ENV_FILE="$2"
    fi

    local VAR=$(grep ^$1= "$ENV_FILE" | xargs)
    IFS="=" read -ra VAR <<< "$VAR"
    echo ${VAR[1]}
}

# Ожедание запуска базы данных.
wait_for_port() {
    local name="$1" host="$2" port="$3"
    local j=0
    while ! nc -z "$host" "$port" >/dev/null 2>&1 < /dev/null; do
        j=$((j+1))
        if [ $j -ge $TRY_LOOP ]; then
            echo >&2 "$(date) - $host:$port still not reachable, giving up"
            exit 1
        fi
        echo "$(date) - waiting for $name... $j/$TRY_LOOP"
        sleep 5
    done
}

# Запуск сервера grandcore.
run(){
    gunicorn backend.wsgi:application -w 4 -b 0.0.0.0:8000
}

# Запуск отладки  сервера grandcore.
debug(){
    python3 manage.py runserver --noreload --nothreading 0.0.0.0:8000
}

# Запустить тесты.
test(){
    python manage.py test
}

# Создание новых миграций на основе изменений.
migrations(){
    python manage.py makemigrations $1
}

# Применение миграций.
migrate(){
    python manage.py migrate $1
}

# Выполним подготовку/сборку статических файлов.
collectstatic(){
    python manage.py collectstatic --no-input
}

# Читаем переменные из файла.
SQL_DATABASE=$(read_var SQL_DATABASE .env)
SQL_HOST=$(read_var SQL_HOST .env)
SQL_PORT=$(read_var SQL_PORT .env)

DEBUG=$(read_var DEBUG .env)


#============================================================================
case $1 in
    run)
        wait_for_port "$SQL_DATABASE" "$SQL_HOST" "$SQL_PORT"
        echo ""
        echo "==================================================="
        echo "=          Migrations models grandcore.           ="
        echo "==================================================="
        migrations

        echo ""
        echo "==================================================="
        echo "=          Migrate models grandcore.              ="
        echo "==================================================="
        migrate "auth"
        migrate "--run-syncdb --no-input"

        echo ""
        echo "==================================================="
        echo "=         Let's prepare/build static files.       ="
        echo "==================================================="
        collectstatic

        echo ""
        echo "==================================================="
        echo "=             Run server grandcore.               ="
        echo "==================================================="
        if [[ "$DEBUG" == "True" ]]; then
            echo "Run server for Debug!"
            debug $2
        else
            run $2
        fi
    ;;
    test)
        echo ""
        echo "==================================================="
        echo "=     Launching the testing project grandcore.    ="
        echo "==================================================="
        test $2
    ;;
    migrations)
        echo ""
        echo "==================================================="
        echo "=          Migrations models grandcore.           ="
        echo "==================================================="
        migrations $2
    ;;
    migrate)
        echo ""
        echo "==================================================="
        echo "=          Migrate models grandcore.              ="
        echo "==================================================="
        migrate $2
    ;;
    collectstatic)
        echo ""
        echo "==================================================="
        echo "=         Let's prepare/build static files.       ="
        echo "==================================================="
        collectstatic $2
    ;;
    *) echo "Invalid option: $1"
    ;;
esac

exit 0
