from rest_framework.serializers import ModelSerializer
from donations.models import Donation
from .projects import ProjectSerializer

class DonationSerializer(ModelSerializer):

    for_project = ProjectSerializer()

    class Meta:
        model = Donation
        fields = "__all__"