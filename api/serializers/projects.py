from rest_framework.serializers import ModelSerializer
from projects.models import Project, ProjectCategory
from .accounts import AccountSerializer


class ProjectCategorySerializer(ModelSerializer):

    class Meta:
        model = ProjectCategory
        fields = ('id', 'name')

class ProjectSerializer(ModelSerializer):

    category = ProjectCategorySerializer()
    author = AccountSerializer()
    curators = AccountSerializer(many=True)

    class Meta:
        model = Project
        fields = "__all__"