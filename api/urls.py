from api.views.accounts import AccountViewSet
from api.views.invites import InviteViewSet, InviteRegistrationViewSet
from api.views.projects import ProjectViewSet
from api.views.donations import DonationViewSet

from rest_framework.routers import DefaultRouter
from django.urls import path
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from .views.accounts import account_registration_handler
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi


schema_view = get_schema_view(
   openapi.Info(
      title="Snippets API",
      default_version='v1',
      description="API for project GrandCore",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="contact@snippets.local"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

router = DefaultRouter()
router.register('user', AccountViewSet)
router.register('invite', InviteViewSet)
router.register('invite_reg', InviteRegistrationViewSet)
router.register('project', ProjectViewSet)
router.register('donation', DonationViewSet)

jwturlpatterns = [
    path('/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]

urlpatterns = [
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    path('register_user', account_registration_handler),
] + router.urls  + jwturlpatterns
