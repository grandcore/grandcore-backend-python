from donations.models import Donation
from api.serializers.donations import DonationSerializer
from rest_framework.pagination import PageNumberPagination
from rest_framework.viewsets import ModelViewSet


class LargeResultsSetPagination(PageNumberPagination):
    page_size = 5


class DonationViewSet(ModelViewSet):
    serializer_class = DonationSerializer
    queryset = Donation.objects.all()
    pagination_class = LargeResultsSetPagination