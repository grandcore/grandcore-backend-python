from django.db import models
from django.core.validators import MinValueValidator
from projects.models import Project
from accounts.models import Account


class Donation(models.Model):

    user = models.ForeignKey(
        Account,
        verbose_name='участник',
        related_name='donations',
        on_delete=models.CASCADE
    )

    amount = models.DecimalField(
        verbose_name='сумма пожертвования',
        default=100,
        decimal_places=2,
        max_digits=9,
        validators=[MinValueValidator(1.0)])  # in USD

    comment = models.TextField(
        blank=True,
        max_length=1000,
        verbose_name='комментарий'
    )

    for_project = models.ForeignKey(
        Project,
        verbose_name='проекту',
        on_delete=models.PROTECT
    )

    created = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Создано'
    )

    class Meta:
        verbose_name = 'пожертвование'
        verbose_name_plural = 'пожертвования'

    def __str__(self):
        return ' '.join([
            str(self.amount),
            self.comment
        ])


class FoundMoneyAllTimeCounter(models.Model):
    value = models.SmallIntegerField(
        verbose_name='сумма'
    )

    created = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Создано'
    )

    class Meta:
        verbose_name = 'всего в фонде'
        verbose_name_plural = 'всего в фонде'

    def __str__(self):
        return str(self.value) or ''


class NowMoneyAllTimeCounter(models.Model):
    value = models.SmallIntegerField(
        verbose_name='сумма'
    )

    created = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Создано'
    )

    class Meta:
        verbose_name = 'текущий фонд'
        verbose_name_plural = 'текущий фонд'

    def __str__(self):
        return str(self.value) or ''


class DeficitMoneyCounter(models.Model):
    value = models.SmallIntegerField(
        verbose_name='сумма'
    )

    created = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Создано'
    )

    class Meta:
        verbose_name = 'дефицит фонд'
        verbose_name_plural = 'дефицит фонд'

    def __str__(self):
        return str(self.value) or ''
