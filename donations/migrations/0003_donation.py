# Generated by Django 3.0.8 on 2020-09-13 22:06

from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('projects', '0010_auto_20200908_2054'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('donations', '0002_delete_donation'),
    ]

    operations = [
        migrations.CreateModel(
            name='Donation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amount', models.DecimalField(decimal_places=2, default=100, max_digits=9, validators=[django.core.validators.MinValueValidator(1.0)], verbose_name='сумма пожертвования')),
                ('comment', models.TextField(blank=True, max_length=1000, verbose_name='комментарий')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Создано')),
                ('for_project', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='projects.Project', verbose_name='проекту')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='участник')),
            ],
            options={
                'verbose_name': 'пожертвование',
                'verbose_name_plural': 'пожертвования',
            },
        ),
    ]
