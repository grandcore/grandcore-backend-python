# Generated by Django 3.0.8 on 2020-09-19 18:25

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('donations', '0003_donation'),
    ]

    operations = [
        migrations.CreateModel(
            name='DeficitMoneyCounter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.SmallIntegerField(verbose_name='сумма')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Создано')),
            ],
            options={
                'verbose_name': 'дефицит фонд',
                'verbose_name_plural': 'дефицит фонд',
            },
        ),
        migrations.CreateModel(
            name='FoundMoneyAllTimeCounter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.SmallIntegerField(verbose_name='сумма')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Создано')),
            ],
            options={
                'verbose_name': 'всего в фонде',
                'verbose_name_plural': 'всего в фонде',
            },
        ),
        migrations.CreateModel(
            name='NowMoneyAllTimeCounter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.SmallIntegerField(verbose_name='сумма')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Создано')),
            ],
            options={
                'verbose_name': 'текущий фонд',
                'verbose_name_plural': 'текущий фонд',
            },
        ),
        migrations.AlterField(
            model_name='donation',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='donations', to=settings.AUTH_USER_MODEL, verbose_name='участник'),
        ),
    ]
