from django.contrib import admin
from .models import Donation, DeficitMoneyCounter, FoundMoneyAllTimeCounter, NowMoneyAllTimeCounter


class DonationAdmin(admin.ModelAdmin):
    list_display = ('amount', 'comment', 'user')
    search_fields = ('amount', 'comment')
    ordering = ('created',)


class DeficitMoneyCounterAdmin(admin.ModelAdmin):
    list_display = ('value', 'created')
    list_filter = ['created']
    ordering = ('created',)


class FoundMoneyAllTimeCounterAdmin(admin.ModelAdmin):
    list_display = ('value', 'created')
    list_filter = ['created']
    ordering = ('created',)


class NowMoneyAllTimeCounterAdmin(admin.ModelAdmin):
    list_display = ('value', 'created')
    list_filter = ['created']
    ordering = ('created',)


admin.site.register(Donation, DonationAdmin)
admin.site.register(DeficitMoneyCounter, DeficitMoneyCounterAdmin)
admin.site.register(FoundMoneyAllTimeCounter, FoundMoneyAllTimeCounterAdmin)
admin.site.register(NowMoneyAllTimeCounter, NowMoneyAllTimeCounterAdmin)