FROM python:alpine

# This prevents Python from writing out pyc files
ENV PYTHONDONTWRITEBYTECODE 1

# This keeps Python from buffering stdin/stdout
ENV PYTHONUNBUFFERED 1

# Setup
RUN apk update
RUN apk upgrade
RUN apk add --update\  
    bash            \
    postgresql-dev  \
    gcc             \
    python3-dev     \
    musl-dev        \
    jpeg-dev        \
    zlib-dev

ENV APP_DIR /var/www/grandcore

# Initialize
RUN mkdir -p ${APP_DIR}
WORKDIR ${APP_DIR}
ADD ./requirements.txt .

# Installing packages
RUN pip3 install --upgrade pip
RUN pip3 install -r requirements.txt

# Clean
RUN apk del -r python3-dev postgresql

# Copy all
ADD . .
RUN chmod -R ugo+x ./entrypoint.sh

EXPOSE 8000

ENTRYPOINT ["./entrypoint.sh"]

