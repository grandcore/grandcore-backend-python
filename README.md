# grandcore-backend-python

_Первая в Мире Open Source корпорация. Универсальная платформа для любых открытых проектов_

- Задачи в Trello - https://trello.com/b/FOFufZWA/backend-python-mvp
- Тимлид1 - https://t.me/kdkulakov
- Тимлид2 - https://t.me/volchugin
- Рабочий чат Бекендеров - https://t.me/joinchat/OQ6Da0w3cCxnfGl9K2Tv9g
- Рабочий чат Фронтендеров (React) - https://t.me/joinchat/OQ6DaxmA5Xf7GERw8oZO8g

- Репозиторий фронтенда на React https://gitlab.com/grandcore/grandcore-front-react/

- Наш сайт - https://grandcore.org
- Наши новости - https://t.me/grandcore_news
- Публичный чат - https://t.me/grandcore_chat
- Написать основателю - https://t.me/grandcore **(если хотите в команду)**

Используем Django 3 + DRF + JWT tokes

Основной URL работы с пользователями /user/. По данному урлу можно получить:

- сипсок пользователей
- конкретного юзера по ID
- добавить пользователя
- удаление пользователя
- редактирование пользователя

Корень сервера - это список доступных URL для работы с API. Можно прям в WEB интерфейсе поиграться с API`шкой.

**Быстрый запуск сервера на Docker контейнерах**

Для развёртывания Бекенда на локальной машине, требуется установленные следующие пакеты/зависимости: `docker` и `docker-compose`.

1. Создайте в корне проекта файл `.env` с переменными. Можно воспользоваться файлом `.env.example` скопировав его содержимое в `.env` файл. Закоменченные переменные в файле `.env.example` не я зляются обязательными и имеют теже значения по умолчанию. Т.е. устанавливаются по умолчанию если не заданы. Если требуется изменить интересующей Вас переменной раскоментируйте её и задайте нужные значения.

2. Для сборки и запуска сервера воспользуйтесь командой:

   `sudo docker-compose up --build`

   или для запуска в фоновом режиме

   `sudo docker-compose up --build -d`

   После всего сервер должен быть доступен по url-ам http://127.0.0.1, http://0.0.0.0 или http://localhost.

3. Для того, что бы задать пароль администратора воспользуйтесь командой:

   `sudo docker exec -it backend python3 manage.py createsuperuser`

4. Для остановки сервера и удаления контейнеров воспользуйтесь командой:

   `docker-compose down -v`

**Быстрый запуск сервера вне Docker контейнеров**

1. Создайте в корне проекта файл `.env` с переменными.

2. Ставим зависимости

   `pip3 install -r requirements.txt`

3. Создание новых миграций на основе изменений

   `python3 manage.py makemigrations`

4. Применение миграций БД для разработки

   `python3 manage.py migrate`

5. Выполним подготовку/сборку статических файлов для фронтенда на Django

   `python3 manage.py collectstatic`.

6. Создаем администратора

   `python3 manage.py createsuperuser`

7. Запускаем сервер

   `python3 manage.py runserver`

После всего сервер должен быть доступен по url-ам http://127.0.0.1, http://0.0.0.0 или http://localhost.

Путь к админке джанги http://127.0.0.1/admin.

**Безопасность**

Используем simple jwt token полное описание есть тут https://django-rest-framework-simplejwt.readthedocs.io/en/latest/getting_started.html

- получение токена - отправляем на url http://127.0.0.1/api/token/ POST запрос с типом контента
  json, c полями email, password

      Пример тела запроса

  `{ "email": "admin@ex.com", "password": "123" }`

      Ответом будет json c двумя токенами, один для рефреша, второй для авторизации `{
      "refresh": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTU5NjMwODYxMiwianRpIjoiMzFlMTdjZGJkODc1NDU5NmJhOGNhZmI2MDkyZTcwYjUiLCJ1c2VyX2lkIjoxfQ.vajxFI4egee_v1sov37VU7166mcDPj5tKJuzM3Oq3lo",
      "access": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTk2MjIyNTEyLCJqdGkiOiJlOGNhNGQ1OGMyMTU0Y2FjOGFiZGUwMDUyNmJjOWE5NiIsInVzZXJfaWQiOjF9.V_Wh-GxNQDJZEYcfV9vB4lb0srJpdBt7RcIeaxQoXRU"

  }`

Токены имеют свойства "протухать" - наш токен закончит действовать через 3000 сек.

    'ACCESS_TOKEN_LIFETIME': timedelta(minutes=5),

    'REFRESH_TOKEN_LIFETIME': timedelta(days=1),

По этому нужно его обновить до истечении этого времени.

- обновление токена - отправляем POST запрос на url http://127.0.0.1/api/token/refresh/

      пример запроса `{
      "refresh": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTU5NjMwODYxMiwianRpIjoiMzFlMTdjZGJkODc1NDU5NmJhOGNhZmI2MDkyZTcwYjUiLCJ1c2VyX2lkIjoxfQ.vajxFI4egee_v1sov37VU7166mcDPj5tKJuzM3Oq3lo"

  }`

      получаем новый токен для авторизации запросов к бэку `{
      "access": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTk2MjIyNTgxLCJqdGkiOiJhM2YwODgxNzk0Y2M0ODFjOTQ4ZmY5ZDkzOTAxODE5NSIsInVzZXJfaWQiOjF9.pzzZy5AghN67KFeTv_rDqh6l-QT0HlgNJc2nmD9f-zw"

  }`

Дальше после получения токена можно получить например информацию о пользователе.
Отсылаем на url http://127.0.0.1/user/ POST запрос в виде:

- в боди пишем `{ "email": "admin@ex.com" }`
- в хедер передаем `Authorization со значением Bearer tut_nash_token`

**Routes**

Роут для регистрации пользовтаеля по инвайту http://127.0.0.1/register_user

Отправляем туда POST запрос в котором должны быть указаны email, password, invited
выглядит это как-то так если использовтаь программу http `http POST http://127.0.0.1/register_user email=user1@exam.com password=123qweASDF invite=acec2c20-3137-478f-bf5d-d94867dda427`

В ответ получите id ник пользователя, по которому потом может забрать данные пользователя (CRUD).
