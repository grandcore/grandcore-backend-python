from django.template import loader
from django.http import HttpResponse
from projects.models import Project
from accounts.models import Account
from donations.models import Donation, NowMoneyAllTimeCounter, FoundMoneyAllTimeCounter, DeficitMoneyCounter
from django.db.models import Sum


def about_page(request):
    template = loader.get_template('about.html')
    context = {'text': 'HELLO'}
    return HttpResponse(template.render(context, request))


def home_page(request):
    template = loader.get_template('home.html')
    projects = Project.objects.filter().order_by('-created')[:6]
    accounts = Account.objects.filter(is_active=True, is_deleted=False,)[:6]
    donations = Donation.objects.filter().order_by('-created')[:6]

    # counters #
    platform_users = Account.objects.filter(is_active=True, is_deleted=False,).count()
    platform_projects = Project.objects.filter().order_by('-created').count()
    found_money_all_time = FoundMoneyAllTimeCounter.objects.filter().order_by('-created').last()
    now_money_all_time = NowMoneyAllTimeCounter.objects.filter().order_by('-created').last()
    deficit_money_сounter = DeficitMoneyCounter.objects.filter().order_by('-created').last()
    context = {
        'projects': projects,
        'accounts': accounts,
        'platform_users': platform_users,
        'platform_projects': platform_projects,
        'donations': donations,
        'found_money_all_time': found_money_all_time,
        'now_money_all_time': now_money_all_time,
        'deficit_money_сounter': deficit_money_сounter,
    }
    return HttpResponse(template.render(context, request))
