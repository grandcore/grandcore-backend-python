from django.apps import AppConfig


class ProjectsConfig(AppConfig):
    name = 'projects'
    verbose_name = 'Проекты'
    verbose_name_plural = 'Проекты'

