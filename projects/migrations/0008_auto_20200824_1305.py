# Generated by Django 3.0.8 on 2020-08-24 10:05

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('projects', '0007_auto_20200823_1202'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='curators',
            field=models.ManyToManyField(blank=True, null=True, related_name='project_curators', to=settings.AUTH_USER_MODEL, verbose_name='кураторы проекта'),
        ),
    ]
