# Generated by Django 3.0.8 on 2020-08-18 02:13

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import mptt.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='ProjectCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True, verbose_name='название категории')),
                ('lft', models.PositiveIntegerField(editable=False)),
                ('rght', models.PositiveIntegerField(editable=False)),
                ('tree_id', models.PositiveIntegerField(db_index=True, editable=False)),
                ('level', models.PositiveIntegerField(editable=False)),
                ('parent', mptt.fields.TreeForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='children', to='projects.ProjectCategory', verbose_name='родительская категория')),
            ],
            options={
                'verbose_name': 'категория',
                'verbose_name_plural': 'категории проекта',
            },
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='дата создания')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='дата последней правки')),
                ('title', models.CharField(max_length=100, verbose_name='название проекта')),
                ('description_brief', models.CharField(blank=True, default='', max_length=256, verbose_name='краткое описание')),
                ('description_full', models.TextField(blank=True, default='', verbose_name='история проекта')),
                ('amount_needed', models.PositiveIntegerField(blank=True, default=None, null=True, verbose_name='необходимая сумма')),
                ('amount_available', models.PositiveIntegerField(blank=True, default=None, null=True, verbose_name='сумма в наличии')),
                ('image', models.ImageField(blank=True, upload_to='projects/uploads', verbose_name='лого')),
                ('file', models.FileField(blank=True, upload_to='projects/uploads', verbose_name='документы')),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='автор проекта')),
                ('category', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='projects.ProjectCategory', verbose_name='категория')),
            ],
            options={
                'verbose_name': 'проект',
                'verbose_name_plural': 'проекты',
            },
        ),
    ]
