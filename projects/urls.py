from django.urls import path
from .views import (
	ProjectListView, 
	# projects_from_category,
	ProjectDetailView, 
	projects_for_category,
)

app_name = 'projects'

urlpatterns = [
    path('', ProjectListView.as_view(), name="projects_list"),
    path('<int:pk>/', ProjectDetailView.as_view(), name="project"),
    # path('<slug:category>/', projects_from_category, name="projects_category"),
    path('category_id=<int:category_id>/', projects_for_category, name="projects_for_category"),
]
