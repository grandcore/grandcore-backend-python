from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.db.models import Q
from .models import Project, ProjectCategory
from django.template.response import TemplateResponse


class ProjectListView(ListView):
    model = Project
    template_name = 'projects/projects_list.html'
    context_object_name = 'projects'

    def get_queryset(self):
        query = self.request.GET.get('q')
        if not query == None:
            projects = Project.objects.filter(
                Q(title__icontains=query) |
                Q(description_brief__icontains=query) |
                Q(category__name__icontains=query)
            ).order_by('-created')
        else:
            projects = Project.objects.order_by('-created')
        return projects

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['category'] = ProjectCategory.objects.all()
        return context


class ProjectCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Project
    # form_class = ProjectCreateForm
    template_name = "projects/create-project.html"
    success_url = "/projects"
    success_message = "project %(title)s was successfully created"


class ProjectEditView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Project
    # form_class = ProjectUpdateForm
    template_name = "projects/edit-project.html"
    success_url = "/projects"
    success_message = "project %(title)s was successfully updated"


class ProjectDetailView(LoginRequiredMixin, SuccessMessageMixin, DetailView):
    model = Project
    # form_class = ProjectUpdateForm
    template_name = "projects/detail-project.html"
    # success_url = "/projects"
    success_message = "project %(title)s was successfully updated"


class ProjectDeleteView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    model = Project
    template_name = "projects/delete-project.html"
    success_url = "/projects"
    success_message = "project %(title)s was successfully deleted"


# def projects_from_category(request, category):
#     template = 'projects/projects_list.html'
#     all_categories = ProjectCategory.objects.all()
#     category_inst = ProjectCategory.objects.get(slug=category)
#     category_projects = Project.objects.filter(category=category_inst)

#     context = {
#         'category': category,
#         'all_categories': all_categories,
#         'projects': category_projects,
#     }
#     return TemplateResponse(request, template, context)


def projects_for_category(request, category_id):
    template = 'projects/projects_for_category.html'
    all_categories = ProjectCategory.objects.all()
    category_tree = ProjectCategory.objects.get(id=category_id).get_descendants(include_self=True)
    ids = [i.id for i in category_tree]
    category_projects = Project.objects.filter(category__id__in=ids)

    context = {
        'category_id': category_id,
        'all_categories': all_categories,
        'projects': category_projects,
    }
    return TemplateResponse(request, template, context)
