from django.views.generic import ListView
from accounts.models import Account


class UsersList(ListView):
    queryset = Account.objects.filter(is_active=True)
    template_name = 'accounts/users_list.html'
    context_object_name = 'users'
    paginate_by = 2


