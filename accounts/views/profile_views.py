from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import View, FormView, DetailView, ListView, UpdateView, CreateView, DeleteView
from ..models import Account, Activation
from ..forms import ProfileForm, CreateForm
from django.urls import reverse_lazy
from projects.models import Project
from donations.models import Donation


class MyProfile(LoginRequiredMixin, DetailView):
    model = Account
    context_object_name = 'user'
    template_name = 'accounts/profile.html'
    form_class = ProfileForm
    success_url = reverse_lazy('accounts:myprofile')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        user_instance = Account.objects.get(pk=self.get_object().pk)

        user_founded_projects = Project.objects.filter(author=user_instance)

        projects_ids = user_founded_projects.values_list('pk', flat=True)
        user_supported_projects = Donation.objects.filter(user=user_instance, for_project__in=list(projects_ids))

        context['user_founded_projects'] = user_founded_projects

        context['user_supported_projects'] = user_supported_projects

        return context

    def get_success_url(self):
        return reverse_lazy('accounts:myprofile', kwargs={'pk': self.object.pk})


class MyProfileUpdate(LoginRequiredMixin, UpdateView):
    model = Account
    template_name = 'accounts/profileupdate.html'
    form_class = ProfileForm
    success_url = reverse_lazy('accounts:profileupdate')

    def get_success_url(self):
        return reverse_lazy('accounts:profileupdate', kwargs={'pk': self.object.pk})

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data()
    #
    #     user_orders = Order.objects.filter(client=self.object.pk).order_by('-created')
    #     context['user_orders'] = user_orders
    #
    #     print(context)
    #     return context

    # def form_valid(self, request, form):
    #     user = request.user
    #
    #     if request.method == "POST":
    #         form = ProfileForm(request.POST)
    #         if form.is_valid():
    #             obj = form.save(commit=False)
    #             obj.user = request.user
    #             obj.save()
    #             # Without this next line the tags won't be saved.
    #             form.save_m2m()