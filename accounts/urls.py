from django.urls import path
from .views.profile_views import MyProfile, MyProfileUpdate
from .views.users_views import UsersList
from .views.sign_views import (
    LogInView, SignUpView, ActivateView, LogOutView,
    ChangeEmailView, ChangeEmailActivateView, ChangeProfileView, ChangePasswordView,
    RestorePasswordView, RestorePasswordDoneView, RestorePasswordConfirmView
)


app_name = 'accounts'


urlpatterns = [
    path('login/', LogInView.as_view(), name='login'),
    path('logout/', LogOutView.as_view(), name='logout'),
    #
    # path('resend/activation-code/', ResendActivationCodeView.as_view(), name='resend_activation_code'),
    #
    path('sign-up/', SignUpView.as_view(), name='sign_up'),
    path('activate/<code>/', ActivateView.as_view(), name='activate'),
    #
    path('restore/password/', RestorePasswordView.as_view(), name='restore_password'),
    path('restore/password/done/', RestorePasswordDoneView.as_view(), name='restore_password_done'),
    path('restore/<uidb64>/<token>/', RestorePasswordConfirmView.as_view(), name='restore_password_confirm'),
    #
    #
    path('change/profile/', ChangeProfileView.as_view(), name='change_profile'),
    path('change/password/', ChangePasswordView.as_view(), name='change_password'),
    path('change/email/', ChangeEmailView.as_view(), name='change_email'),
    path('change/email/<code>/', ChangeEmailActivateView.as_view(), name='change_email_activation'),
    path('<int:pk>', MyProfile.as_view(), name='myprofile'),
    path('profileupdate/<int:pk>', MyProfileUpdate.as_view(), name='profileupdate'),
    # path('users/', AllUsersList.as_view(), name='users'),
    # path('create/', CreateUser.as_view(), name='create'),
    # path('delete/<int:pk>', DeleteUser.as_view(), name='delete'),
    # path('detail/<int:pk>', DetailUser.as_view(), name='detail'),
    # path('update/<int:pk>', UserUpdate.as_view(), name='update'),
    # path('mygrouplist/', MyGroupList.as_view(), name='mygrouplist'),
    path('users/', UsersList.as_view(), name='users'),
]
